package com.example.hw13b;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnSendMessage = findViewById(R.id.sendMessage);
        EditText enteredText = findViewById(R.id.editText);

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String textValue = enteredText.getText().toString();

                Intent intent = new Intent("com.example.MY_ACTION");
                intent.putExtra("com.example.MY_MESSAGE", textValue);
                sendBroadcast(intent);
            }
        });

    }
}